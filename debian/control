Source: kinfocenter
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Patrick Franz <deltaone@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               dh-sequence-kf5,
               extra-cmake-modules (>= 5.97~),
               gettext,
               libegl-dev,
               libkf5auth-dev (>= 5.97~),
               libkf5config-dev (>= 5.97~),
               libkf5configwidgets-dev (>= 5.97~),
               libkf5coreaddons-dev (>= 5.97~),
               libkf5declarative-dev (>= 5.97~),
               libkf5doctools-dev (>= 5.97~),
               libkf5i18n-dev (>= 5.97~),
               libkf5kcmutils-dev (>= 5.97~),
               libkf5kio-dev (>= 5.97~),
               libkf5package-dev (>= 5.97~),
               libkf5service-dev (>= 5.97~),
               libkf5solid-dev (>= 5.97~),
               libkf5widgetsaddons-dev (>= 5.97~),
               libusb-1.0-0-dev,
               pkg-config,
               qml-module-org-kde-kirigami2,
               qtbase5-dev (>= 5.15.2~),
Standards-Version: 4.6.2
Homepage: https://invent.kde.org/plasma/kinfocenter
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kinfocenter
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kinfocenter.git
Rules-Requires-Root: no

Package: kinfocenter
Architecture: any
Depends: plasma-workspace (>= 4:5.14),
         qml-module-org-kde-kirigami2,
         systemsettings,
         usb.ids,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: aha,
            dmidecode [any-amd64 arm64 armhf ia64 any-i386 riscv64],
            pciutils,
            clinfo,
            mesa-utils,
            vulkan-tools [linux-any],
            wayland-utils,
            x11-utils,
Suggests: samba
Description: system information viewer
 The kinfocenter provides you with a centralized and convenient
 overview of your system and desktop environment.
 .
 The information center is made up of multiple modules.  Each module is a
 separate application, but the information center organizes all of these
 programs into a convenient location.
